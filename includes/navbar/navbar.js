let isMenuOpen = false;
function ToggleMenu() {
    isMenuOpen = !isMenuOpen;
    document.getElementById("menu-icon").src = isMenuOpen ? '../../assets/icons/close-white.svg' : '../../assets/icons/menu-white.svg';
    document.getElementById("menu").classList.toggle('display-block');
}